import type { OpenHouse } from '../models/OpenHouse';
export declare class OpenHouseService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns OpenHouse Successful
     * @throws ApiError
     */
    static getOpenHouseOdata(select?: string, top?: string, filter?: string): Promise<Array<OpenHouse>>;
}

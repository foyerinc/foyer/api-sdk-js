import type { OtherPhone } from '../models/OtherPhone';
export declare class OtherPhoneService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns OtherPhone Successful
     * @throws ApiError
     */
    static getOtherPhoneOdata(select?: string, top?: string, filter?: string): Promise<Array<OtherPhone>>;
}

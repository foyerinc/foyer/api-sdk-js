import type { Prospecting } from '../models/Prospecting';
export declare class ProspectingService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Prospecting Successful
     * @throws ApiError
     */
    static getProspectingOdata(select?: string, top?: string, filter?: string): Promise<Array<Prospecting>>;
}

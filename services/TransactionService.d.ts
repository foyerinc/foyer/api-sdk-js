import type { PatchBody } from '../models/PatchBody';
import type { Transaction } from '../models/Transaction';
export declare class TransactionService {
    /**
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static getTransactions(filter?: string): Promise<Array<Transaction>>;
    /**
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    static postTransactions(requestBody: Transaction): Promise<Transaction>;
    /**
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Transaction>;
    /**
     * @param id
     * @param filter
     * @returns Transaction Successful
     * @throws ApiError
     */
    static getTransactionById(id: number, filter?: string): Promise<Transaction>;
    /**
     * @param id
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    static putTransactionById(id: number, requestBody: Transaction): Promise<Transaction>;
    /**
     * @param id
     * @param requestBody
     * @returns Transaction patched Transaction
     * @throws ApiError
     */
    static patchTransactionById(id: number, requestBody?: PatchBody): Promise<Transaction>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteTransactionById(id: number): Promise<void>;
    /**
     * @param requestBody
     * @returns Transaction Successful
     * @throws ApiError
     */
    static createOrUpdate(requestBody: {
        transaction?: Transaction;
    }): Promise<Transaction>;
}

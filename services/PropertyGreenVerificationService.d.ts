import type { PropertyGreenVerification } from '../models/PropertyGreenVerification';
export declare class PropertyGreenVerificationService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns PropertyGreenVerification Successful
     * @throws ApiError
     */
    static getPropertyGreenVerificationOdata(select?: string, top?: string, filter?: string): Promise<Array<PropertyGreenVerification>>;
}

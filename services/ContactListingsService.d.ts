import type { ContactListings } from '../models/ContactListings';
export declare class ContactListingsService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns ContactListings Successful
     * @throws ApiError
     */
    static getContactListingsOdata(select?: string, top?: string, filter?: string): Promise<Array<ContactListings>>;
}

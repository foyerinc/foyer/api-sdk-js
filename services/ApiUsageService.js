"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiUsageService = void 0;
const request_1 = require("../core/request");
class ApiUsageService {
    /**
     * @param filter
     * @returns ApiUsage array of ApiUsages
     * @throws ApiError
     */
    static getApiUsages(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/ApiUsage`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns ApiUsage created ApiUsage
     * @throws ApiError
     */
    static postApiUsages(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/ApiUsage`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param filter
     * @returns ApiUsage Successful
     * @throws ApiError
     */
    static findOne(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/ApiUsage/findOne`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param filter
     * @returns ApiUsage single ApiUsage
     * @throws ApiError
     */
    static getApiUsageById(id, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/ApiUsage/${id}`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns ApiUsage updated ApiUsage
     * @throws ApiError
     */
    static putApiUsageById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PUT',
                path: `/ApiUsage/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns ApiUsage patched ApiUsage
     * @throws ApiError
     */
    static patchApiUsageById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PATCH',
                path: `/ApiUsage/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteApiUsageById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'DELETE',
                path: `/ApiUsage/${id}`,
            });
            return result.body;
        });
    }
}
exports.ApiUsageService = ApiUsageService;

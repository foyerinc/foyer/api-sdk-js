import type { SavedSearch } from '../models/SavedSearch';
export declare class SavedSearchService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns SavedSearch Successful
     * @throws ApiError
     */
    static getSavedSearchOdata(select?: string, top?: string, filter?: string): Promise<Array<SavedSearch>>;
}

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PropertyService = void 0;
const request_1 = require("../core/request");
class PropertyService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Property Successful
     * @throws ApiError
     */
    static getPropertyOdata(select, top, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/odata/Property`,
                query: {
                    '$select': select,
                    '$top': top,
                    '$filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param filter
     * @returns Property Successful
     * @throws ApiError
     */
    static getProperty(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Property`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param limit
     * @param bufLen
     * @returns Property Successful
     * @throws ApiError
     */
    static getRecommendations(limit, bufLen) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Property/getRecommendations`,
                query: {
                    'limit': limit,
                    'bufLen': bufLen,
                },
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param streetNumber
     * @param street
     * @param streetType
     * @param cityName
     * @param state
     * @param lat
     * @param lng
     * @returns any Successful
     * @throws ApiError
     */
    static findNearest(streetNumber, street, streetType, cityName, state, lat, lng) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Property/findNearest`,
                query: {
                    'streetNumber': streetNumber,
                    'street': street,
                    'streetType': streetType,
                    'cityName': cityName,
                    'state': state,
                    'lat': lat,
                    'lng': lng,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    static calculateLocalGraph(id, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Property/${id}/calculateLocalGraph`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    static calculateScore(id, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Property/${id}/calculateScore`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param filter
     * @returns Property Successful
     * @throws ApiError
     */
    static findOne(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Property/findOne`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param filter
     * @returns Property single Property
     * @throws ApiError
     */
    static getPropertyById(id, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Property/${id}`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns Property updated Property
     * @throws ApiError
     */
    static putPropertyById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PUT',
                path: `/Property/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns Property patched Property
     * @throws ApiError
     */
    static patchPropertyById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PATCH',
                path: `/Property/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deletePropertyById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'DELETE',
                path: `/Property/${id}`,
            });
            return result.body;
        });
    }
}
exports.PropertyService = PropertyService;

import type { PatchBody } from '../models/PatchBody';
import type { Publisher } from '../models/Publisher';
export declare class PublisherService {
    /**
     * @param filter
     * @returns Publisher array of Publishers
     * @throws ApiError
     */
    static getPublishers(filter?: string): Promise<Array<Publisher>>;
    /**
     * @param requestBody
     * @returns Publisher created Publisher
     * @throws ApiError
     */
    static postPublishers(requestBody: Publisher): Promise<Publisher>;
    /**
     * @param filter
     * @returns Publisher Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Publisher>;
    /**
     * @param id
     * @param filter
     * @returns Publisher single Publisher
     * @throws ApiError
     */
    static getPublisherById(id: number, filter?: string): Promise<Publisher>;
    /**
     * @param id
     * @param requestBody
     * @returns Publisher updated Publisher
     * @throws ApiError
     */
    static putPublisherById(id: number, requestBody: Publisher): Promise<Publisher>;
    /**
     * @param id
     * @param requestBody
     * @returns Publisher patched Publisher
     * @throws ApiError
     */
    static patchPublisherById(id: number, requestBody?: PatchBody): Promise<Publisher>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deletePublisherById(id: number): Promise<void>;
    /**
     * @returns string array of urls
     * @throws ApiError
     */
    static refreshAllProperties(): Promise<Array<string>>;
    /**
     * @param requestBody
     * @returns Publisher Successful
     * @throws ApiError
     */
    static updateNotify(requestBody: {
        notify?: string;
    }): Promise<Publisher>;
}

import type { HistoryTransactional } from '../models/HistoryTransactional';
export declare class HistoryTransactionalService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns HistoryTransactional Successful
     * @throws ApiError
     */
    static getHistoryTransactionalOdata(select?: string, top?: string, filter?: string): Promise<Array<HistoryTransactional>>;
}

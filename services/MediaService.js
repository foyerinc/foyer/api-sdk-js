"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MediaService = void 0;
const request_1 = require("../core/request");
class MediaService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMediaOdata(select, top, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/odata/Media`,
                query: {
                    '$select': select,
                    '$top': top,
                    '$filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMedias(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Media`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    static postMedias(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static findOne(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Media/findOne`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @returns Media Successful
     * @throws ApiError
     */
    static getMisclassified() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Media/getMisclassified`,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMediaById(id, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Media/${id}`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    static putMediaById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PUT',
                path: `/Media/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns Media patched Media
     * @throws ApiError
     */
    static patchMediaById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PATCH',
                path: `/Media/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteMediaById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'DELETE',
                path: `/Media/${id}`,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param id
     * @returns number number of affected media
     * @throws ApiError
     */
    static hideById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/${id}/hide`,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param id
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    static updateFromQueue(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/${id}/updateFromQueue`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns any Successful
     * @throws ApiError
     */
    static classify(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/classify`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @returns any Successful
     * @throws ApiError
     */
    static classifyReso() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/classifyReso`,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static reportClassificationError(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/reportClassificationError`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns number number of affected rows
     * @throws ApiError
     */
    static resetByHash(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/resetByHash`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns number number of affected rows
     * @throws ApiError
     */
    static updateByHash(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/updateByHash`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns number number of affected rows
     * @throws ApiError
     */
    static uploadBatch(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/uploadBatch`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns any Successful
     * @throws ApiError
     */
    static validateReso(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Media/validateReso`,
                body: requestBody,
            });
            return result.body;
        });
    }
}
exports.MediaService = MediaService;

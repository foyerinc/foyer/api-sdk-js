import type { PatchBody } from '../models/PatchBody';
import type { Shopper } from '../models/Shopper';
export declare class ShopperService {
    /**
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static getShoppers(filter?: string): Promise<Array<Shopper>>;
    /**
     * @param requestBody
     * @returns Shopper Successful
     * @throws ApiError
     */
    static postShoppers(requestBody: Shopper): Promise<Shopper>;
    /**
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Shopper>;
    /**
     * @param id
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static getShopperById(id: number, filter?: string): Promise<Shopper>;
    /**
     * @param id
     * @param requestBody
     * @returns Shopper Successful
     * @throws ApiError
     */
    static putShopperById(id: number, requestBody: Shopper): Promise<Shopper>;
    /**
     * @param id
     * @param requestBody
     * @returns Shopper patched Shopper
     * @throws ApiError
     */
    static patchShopperById(id: number, requestBody?: PatchBody): Promise<Shopper>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteShopperById(id: number): Promise<void>;
}

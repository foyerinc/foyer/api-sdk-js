import type { ErrorsCalculatingFoyerScore } from '../models/ErrorsCalculatingFoyerScore';
import type { PatchBody } from '../models/PatchBody';
import type { Property } from '../models/Property';
import type { PropertyScore } from '../models/PropertyScore';
export declare class PropertyService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Property Successful
     * @throws ApiError
     */
    static getPropertyOdata(select?: string, top?: string, filter?: string): Promise<Array<Property>>;
    /**
     * @param filter
     * @returns Property Successful
     * @throws ApiError
     */
    static getProperty(filter?: string): Promise<Array<Property>>;
    /**
     * For Foyer internal use.
     * @param limit
     * @param bufLen
     * @returns Property Successful
     * @throws ApiError
     */
    static getRecommendations(limit?: number, bufLen?: number): Promise<Array<Property>>;
    /**
     * For Foyer internal use.
     * @param streetNumber
     * @param street
     * @param streetType
     * @param cityName
     * @param state
     * @param lat
     * @param lng
     * @returns any Successful
     * @throws ApiError
     */
    static findNearest(streetNumber: string, street: string, streetType: string, cityName: string, state: string, lat: number, lng: number): Promise<{
        bestChoice?: Property;
        neighbors?: Array<Property>;
        properties?: Array<Property>;
    }>;
    /**
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    static calculateLocalGraph(id: number, filter?: string): Promise<any>;
    /**
     * @param id
     * @param filter
     * @returns any Successful
     * @throws ApiError
     */
    static calculateScore(id: number, filter?: string): Promise<(PropertyScore | ErrorsCalculatingFoyerScore)>;
    /**
     * @param filter
     * @returns Property Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Property>;
    /**
     * @param id
     * @param filter
     * @returns Property single Property
     * @throws ApiError
     */
    static getPropertyById(id: number, filter?: string): Promise<Property>;
    /**
     * @param id
     * @param requestBody
     * @returns Property updated Property
     * @throws ApiError
     */
    static putPropertyById(id: number, requestBody: Property): Promise<Property>;
    /**
     * @param id
     * @param requestBody
     * @returns Property patched Property
     * @throws ApiError
     */
    static patchPropertyById(id: number, requestBody?: PatchBody): Promise<Property>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deletePropertyById(id: number): Promise<void>;
}

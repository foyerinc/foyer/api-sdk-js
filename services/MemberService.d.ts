import type { Member } from '../models/Member';
import type { PatchBody } from '../models/PatchBody';
export declare class MemberService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Member Successful
     * @throws ApiError
     */
    static getMemberOdata(select?: string, top?: string, filter?: string): Promise<Array<Member>>;
    /**
     * @param filter
     * @returns Member Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Member>;
    /**
     * @param id
     * @param filter
     * @returns Member single Member
     * @throws ApiError
     */
    static getMemberById(id: number, filter?: string): Promise<Member>;
    /**
     * @param id
     * @param requestBody
     * @returns Member updated Member
     * @throws ApiError
     */
    static putMemberById(id: number, requestBody: Member): Promise<Member>;
    /**
     * @param id
     * @param requestBody
     * @returns Member patched Member
     * @throws ApiError
     */
    static patchMemberById(id: number, requestBody?: PatchBody): Promise<Member>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteMemberById(id: number): Promise<void>;
}

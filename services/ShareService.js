"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShareService = void 0;
const request_1 = require("../core/request");
class ShareService {
    /**
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static getShares(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Share`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static postShares(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Share`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static makeShare(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Share/makeShare`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static findOne(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Share/findOne`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static getShareById(id, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Share/${id}`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static putShareById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PUT',
                path: `/Share/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns Share patched Share
     * @throws ApiError
     */
    static patchShareById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PATCH',
                path: `/Share/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteShareById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'DELETE',
                path: `/Share/${id}`,
            });
            return result.body;
        });
    }
}
exports.ShareService = ShareService;

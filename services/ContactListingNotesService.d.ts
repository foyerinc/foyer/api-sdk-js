import type { ContactListingNotes } from '../models/ContactListingNotes';
export declare class ContactListingNotesService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns ContactListingNotes Successful
     * @throws ApiError
     */
    static getContactListingNotesOdata(select?: string, top?: string, filter?: string): Promise<Array<ContactListingNotes>>;
}

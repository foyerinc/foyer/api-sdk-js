import type { OUID } from '../models/OUID';
export declare class OuidService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns OUID Successful
     * @throws ApiError
     */
    static getOuidOdata(select?: string, top?: string, filter?: string): Promise<Array<OUID>>;
}

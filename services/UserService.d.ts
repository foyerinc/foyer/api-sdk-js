import type { Location } from '../models/Location';
import type { PatchBody } from '../models/PatchBody';
import type { User } from '../models/User';
export declare class UserService {
    /**
     * @param filter
     * @returns User array of Users
     * @throws ApiError
     */
    static getUsers(filter?: string): Promise<Array<User>>;
    /**
     * @param requestBody
     * @returns User created User
     * @throws ApiError
     */
    static postUsers(requestBody: User): Promise<User>;
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns User new API consumer User
     * @throws ApiError
     */
    static createFromAdmin(requestBody: {
        email?: string;
        password?: string;
        monthlyAmount?: number;
    }): Promise<User>;
    /**
     * @param requestBody
     * @returns string successful login
     * @throws ApiError
     */
    static login(requestBody: {
        email?: string;
        password?: string;
        source?: string;
    }): Promise<string>;
    /**
     * @param requestBody
     * @returns string successful login
     * @throws ApiError
     */
    static adminLogin(requestBody: {
        email?: string;
        password?: string;
    }): Promise<string>;
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static changePassword(requestBody: {
        currentPassword?: string;
        newPassword?: string;
        confirmNewPassword?: string;
    }): Promise<void>;
    /**
     * @param id
     * @param filter
     * @returns User single User
     * @throws ApiError
     */
    static getUserById(id: number, filter?: string): Promise<User>;
    /**
     * @param id
     * @param requestBody
     * @returns User updated User
     * @throws ApiError
     */
    static putUserById(id: number, requestBody: User): Promise<User>;
    /**
     * @param id
     * @param requestBody
     * @returns User patched User
     * @throws ApiError
     */
    static patchUserById(id: number, requestBody?: PatchBody): Promise<User>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteUserById(id: number): Promise<void>;
    /**
     * For Foyer internal use.
     * @param id
     * @param phone
     * @returns void
     * @throws ApiError
     */
    static requestSmsVerification(id: number, phone: string): Promise<void>;
    /**
     * For Foyer internal use.
     * @param id
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static verifySms(id: number, requestBody: {
        verificationCode?: number;
    }): Promise<void>;
    /**
     * For Foyer internal use.
     * @param id
     * @returns void
     * @throws ApiError
     */
    static revokeTokens(id: number): Promise<void>;
    /**
     * For Foyer internal use.
     * @param id
     * @param requestBody
     * @returns User Successful
     * @throws ApiError
     */
    static updateUserLocation(id: number, requestBody: {
        locationVerified?: boolean;
        location?: Location;
        searchRadius?: number;
        searchCity?: string;
    }): Promise<User>;
    /**
     * @param filter
     * @returns User single User matching filter criteria
     * @throws ApiError
     */
    static findOne(filter: string): Promise<User>;
    /**
     * For Foyer internal use.
     * @returns User all API consumer Users
     * @throws ApiError
     */
    static getConsumers(): Promise<Array<User>>;
    /**
     * @param requestBody
     * @returns any send reset password link in email to user
     * @throws ApiError
     */
    static resetPasswordStepOne(requestBody: {
        email?: string;
    }): Promise<any>;
    /**
     * @param requestBody
     * @returns any Reset password
     * @throws ApiError
     */
    static resetPasswordStepTwo(requestBody: {
        resetToken?: string;
        newPassword?: string;
        confirmNewPassword?: string;
    }): Promise<any>;
    /**
     * @param requestBody
     * @returns any Delete token for sign out
     * @throws ApiError
     */
    static deleteToken(requestBody: {
        token?: string;
    }): Promise<any>;
    /**
     * @param requestBody
     * @returns any User and related models created
     * @throws ApiError
     */
    static signUp(requestBody: {
        firstName?: string;
        lastName?: string;
        email?: string;
        password?: string;
        confirmedPassword?: string;
        source?: string;
    }): Promise<any>;
    /**
     * @param requestBody
     * @returns any user and publisher created
     * @throws ApiError
     */
    static publisherSignUp(requestBody: {
        email?: string;
        password?: string;
        confirmedPassword?: string;
        name?: string;
        website?: string;
        logo?: string;
    }): Promise<any>;
}

import type { Favorite } from '../models/Favorite';
import type { PatchBody } from '../models/PatchBody';
export declare class FavoriteService {
    /**
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static getFavorites(filter?: string): Promise<Array<Favorite>>;
    /**
     * @param requestBody
     * @returns Favorite Successful
     * @throws ApiError
     */
    static postFavorites(requestBody: Favorite): Promise<Favorite>;
    /**
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Favorite>;
    /**
     * @param id
     * @param filter
     * @returns Favorite Successful
     * @throws ApiError
     */
    static getFavoriteById(id: number, filter?: string): Promise<Favorite>;
    /**
     * @param id
     * @param requestBody
     * @returns Favorite Successful
     * @throws ApiError
     */
    static putFavoriteById(id: number, requestBody: Favorite): Promise<Favorite>;
    /**
     * @param id
     * @param requestBody
     * @returns Favorite patched Favorite
     * @throws ApiError
     */
    static patchFavoriteById(id: number, requestBody?: PatchBody): Promise<Favorite>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteFavoriteById(id: number): Promise<void>;
}

import type { PropertyRooms } from '../models/PropertyRooms';
export declare class PropertyRoomsService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns PropertyRooms Successful
     * @throws ApiError
     */
    static getPropertyRoomsOdata(select?: string, top?: string, filter?: string): Promise<Array<PropertyRooms>>;
}

import type { PropertyPowerProduction } from '../models/PropertyPowerProduction';
export declare class PropertyPowerProductionService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns PropertyPowerProduction Successful
     * @throws ApiError
     */
    static getPropertyPowerProductionOdata(select?: string, top?: string, filter?: string): Promise<Array<PropertyPowerProduction>>;
}

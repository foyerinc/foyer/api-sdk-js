import type { Office } from '../models/Office';
import type { PatchBody } from '../models/PatchBody';
export declare class OfficeService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Office Successful
     * @throws ApiError
     */
    static getOfficeOdata(select?: string, top?: string, filter?: string): Promise<Array<Office>>;
    /**
     * @param filter
     * @returns Office Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Office>;
    /**
     * @param id
     * @param filter
     * @returns Office single Office
     * @throws ApiError
     */
    static getOfficeById(id: number, filter?: string): Promise<Office>;
    /**
     * @param id
     * @param requestBody
     * @returns Office updated Office
     * @throws ApiError
     */
    static putOfficeById(id: number, requestBody: Office): Promise<Office>;
    /**
     * @param id
     * @param requestBody
     * @returns Office patched Office
     * @throws ApiError
     */
    static patchOfficeById(id: number, requestBody?: PatchBody): Promise<Office>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteOfficeById(id: number): Promise<void>;
}

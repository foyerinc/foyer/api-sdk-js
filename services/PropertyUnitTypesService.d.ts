import type { PropertyUnitTypes } from '../models/PropertyUnitTypes';
export declare class PropertyUnitTypesService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns PropertyUnitTypes Successful
     * @throws ApiError
     */
    static getPropertyUnitTypesOdata(select?: string, top?: string, filter?: string): Promise<Array<PropertyUnitTypes>>;
}

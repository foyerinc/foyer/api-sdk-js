import type { InternetTracking } from '../models/InternetTracking';
export declare class InternetTrackingService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns InternetTracking Successful
     * @throws ApiError
     */
    static getInternetTrackingOdata(select?: string, top?: string, filter?: string): Promise<Array<InternetTracking>>;
}

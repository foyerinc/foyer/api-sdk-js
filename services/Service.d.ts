export declare class Service {
    /**
     * @returns any OData metadata
     * @throws ApiError
     */
    static getService(): Promise<any>;
}

import type { Queue } from '../models/Queue';
export declare class QueueService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Queue Successful
     * @throws ApiError
     */
    static getQueueOdata(select?: string, top?: string, filter?: string): Promise<Array<Queue>>;
}

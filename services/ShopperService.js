"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShopperService = void 0;
const request_1 = require("../core/request");
class ShopperService {
    /**
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static getShoppers(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Shopper`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns Shopper Successful
     * @throws ApiError
     */
    static postShoppers(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/Shopper`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static findOne(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Shopper/findOne`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param filter
     * @returns Shopper Successful
     * @throws ApiError
     */
    static getShopperById(id, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/Shopper/${id}`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns Shopper Successful
     * @throws ApiError
     */
    static putShopperById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PUT',
                path: `/Shopper/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns Shopper patched Shopper
     * @throws ApiError
     */
    static patchShopperById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PATCH',
                path: `/Shopper/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteShopperById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'DELETE',
                path: `/Shopper/${id}`,
            });
            return result.body;
        });
    }
}
exports.ShopperService = ShopperService;

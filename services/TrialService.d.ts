import type { PatchBody } from '../models/PatchBody';
import type { Trial } from '../models/Trial';
export declare class TrialService {
    /**
     * @param filter
     * @returns Trial Successful
     * @throws ApiError
     */
    static getTrials(filter?: string): Promise<Array<Trial>>;
    /**
     * @param requestBody
     * @returns Trial Successful
     * @throws ApiError
     */
    static postTrials(requestBody: Trial): Promise<Trial>;
    /**
     * @param filter
     * @returns Trial Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Trial>;
    /**
     * @param id
     * @param filter
     * @returns Trial Successful
     * @throws ApiError
     */
    static getTrialById(id: number, filter?: string): Promise<Trial>;
    /**
     * @param id
     * @param requestBody
     * @returns Trial Successful
     * @throws ApiError
     */
    static putTrialById(id: number, requestBody: Trial): Promise<Trial>;
    /**
     * @param id
     * @param requestBody
     * @returns Trial patched Trial
     * @throws ApiError
     */
    static patchTrialById(id: number, requestBody?: PatchBody): Promise<Trial>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteTrialById(id: number): Promise<void>;
}

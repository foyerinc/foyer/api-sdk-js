import type { SocialMedia } from '../models/SocialMedia';
export declare class SocialMediaService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns SocialMedia Successful
     * @throws ApiError
     */
    static getSocialMediaOdata(select?: string, top?: string, filter?: string): Promise<Array<SocialMedia>>;
}

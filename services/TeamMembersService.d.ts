import type { TeamMembers } from '../models/TeamMembers';
export declare class TeamMembersService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns TeamMembers Successful
     * @throws ApiError
     */
    static getTeamMembersOdata(select?: string, top?: string, filter?: string): Promise<Array<TeamMembers>>;
}

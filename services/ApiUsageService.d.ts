import type { ApiUsage } from '../models/ApiUsage';
import type { PatchBody } from '../models/PatchBody';
export declare class ApiUsageService {
    /**
     * @param filter
     * @returns ApiUsage array of ApiUsages
     * @throws ApiError
     */
    static getApiUsages(filter?: string): Promise<Array<ApiUsage>>;
    /**
     * @param requestBody
     * @returns ApiUsage created ApiUsage
     * @throws ApiError
     */
    static postApiUsages(requestBody: ApiUsage): Promise<ApiUsage>;
    /**
     * @param filter
     * @returns ApiUsage Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<ApiUsage>;
    /**
     * @param id
     * @param filter
     * @returns ApiUsage single ApiUsage
     * @throws ApiError
     */
    static getApiUsageById(id: number, filter?: string): Promise<ApiUsage>;
    /**
     * @param id
     * @param requestBody
     * @returns ApiUsage updated ApiUsage
     * @throws ApiError
     */
    static putApiUsageById(id: number, requestBody: ApiUsage): Promise<ApiUsage>;
    /**
     * @param id
     * @param requestBody
     * @returns ApiUsage patched ApiUsage
     * @throws ApiError
     */
    static patchApiUsageById(id: number, requestBody?: PatchBody): Promise<ApiUsage>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteApiUsageById(id: number): Promise<void>;
}

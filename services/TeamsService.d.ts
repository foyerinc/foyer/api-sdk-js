import type { Teams } from '../models/Teams';
export declare class TeamsService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Teams Successful
     * @throws ApiError
     */
    static getTeamsOdata(select?: string, top?: string, filter?: string): Promise<Array<Teams>>;
}

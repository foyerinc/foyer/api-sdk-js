import type { Showing } from '../models/Showing';
export declare class ShowingService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Showing Successful
     * @throws ApiError
     */
    static getShowingOdata(select?: string, top?: string, filter?: string): Promise<Array<Showing>>;
}

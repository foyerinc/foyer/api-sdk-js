"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserService = void 0;
const request_1 = require("../core/request");
class UserService {
    /**
     * @param filter
     * @returns User array of Users
     * @throws ApiError
     */
    static getUsers(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/User`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns User created User
     * @throws ApiError
     */
    static postUsers(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns User new API consumer User
     * @throws ApiError
     */
    static createFromAdmin(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/createFromAdmin`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns string successful login
     * @throws ApiError
     */
    static login(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/login`,
                body: requestBody,
                responseHeader: 'authorization',
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns string successful login
     * @throws ApiError
     */
    static adminLogin(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/adminLogin`,
                body: requestBody,
                responseHeader: 'authorization',
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static changePassword(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/changePassword`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param filter
     * @returns User single User
     * @throws ApiError
     */
    static getUserById(id, filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/User/${id}`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns User updated User
     * @throws ApiError
     */
    static putUserById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PUT',
                path: `/User/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @param requestBody
     * @returns User patched User
     * @throws ApiError
     */
    static patchUserById(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'PATCH',
                path: `/User/${id}`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteUserById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'DELETE',
                path: `/User/${id}`,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param id
     * @param phone
     * @returns void
     * @throws ApiError
     */
    static requestSmsVerification(id, phone) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/User/${id}/requestSMSVerification`,
                query: {
                    'phone': phone,
                },
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param id
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static verifySms(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/${id}/verifySMS`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param id
     * @returns void
     * @throws ApiError
     */
    static revokeTokens(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/${id}/revokeTokens`,
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @param id
     * @param requestBody
     * @returns User Successful
     * @throws ApiError
     */
    static updateUserLocation(id, requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/${id}/updateUserLocation`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param filter
     * @returns User single User matching filter criteria
     * @throws ApiError
     */
    static findOne(filter) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/User/findOne`,
                headers: {
                    'filter': filter,
                },
            });
            return result.body;
        });
    }
    /**
     * For Foyer internal use.
     * @returns User all API consumer Users
     * @throws ApiError
     */
    static getConsumers() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'GET',
                path: `/User/getConsumers`,
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns any send reset password link in email to user
     * @throws ApiError
     */
    static resetPasswordStepOne(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/resetPasswordStepOne`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns any Reset password
     * @throws ApiError
     */
    static resetPasswordStepTwo(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/resetPasswordStepTwo`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns any Delete token for sign out
     * @throws ApiError
     */
    static deleteToken(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/deleteToken`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns any User and related models created
     * @throws ApiError
     */
    static signUp(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/signUp`,
                body: requestBody,
            });
            return result.body;
        });
    }
    /**
     * @param requestBody
     * @returns any user and publisher created
     * @throws ApiError
     */
    static publisherSignUp(requestBody) {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield (0, request_1.request)({
                method: 'POST',
                path: `/User/publisherSignup`,
                body: requestBody,
            });
            return result.body;
        });
    }
}
exports.UserService = UserService;

import type { Rules } from '../models/Rules';
export declare class RulesService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Rules Successful
     * @throws ApiError
     */
    static getRulesOdata(select?: string, top?: string, filter?: string): Promise<Array<Rules>>;
}

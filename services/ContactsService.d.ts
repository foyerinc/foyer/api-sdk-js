import type { Contacts } from '../models/Contacts';
export declare class ContactsService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Contacts Successful
     * @throws ApiError
     */
    static getContactsOdata(select?: string, top?: string, filter?: string): Promise<Array<Contacts>>;
}

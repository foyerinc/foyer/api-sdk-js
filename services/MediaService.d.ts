import type { BoundingBox } from '../models/BoundingBox';
import type { Media } from '../models/Media';
import type { MediaPipelinePayload } from '../models/MediaPipelinePayload';
import type { PatchBody } from '../models/PatchBody';
export declare class MediaService {
    /**
     * @param select
     * @param top
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMediaOdata(select?: string, top?: string, filter?: string): Promise<Array<Media>>;
    /**
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMedias(filter?: string): Promise<Array<Media>>;
    /**
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    static postMedias(requestBody: Media): Promise<Media>;
    /**
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Media>;
    /**
     * For Foyer internal use.
     * @returns Media Successful
     * @throws ApiError
     */
    static getMisclassified(): Promise<Array<Media>>;
    /**
     * @param id
     * @param filter
     * @returns Media Successful
     * @throws ApiError
     */
    static getMediaById(id: number, filter?: string): Promise<Media>;
    /**
     * @param id
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    static putMediaById(id: number, requestBody: Media): Promise<Media>;
    /**
     * @param id
     * @param requestBody
     * @returns Media patched Media
     * @throws ApiError
     */
    static patchMediaById(id: number, requestBody?: PatchBody): Promise<Media>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteMediaById(id: number): Promise<void>;
    /**
     * For Foyer internal use.
     * @param id
     * @returns number number of affected media
     * @throws ApiError
     */
    static hideById(id: number): Promise<number>;
    /**
     * For Foyer internal use.
     * @param id
     * @param requestBody
     * @returns Media Successful
     * @throws ApiError
     */
    static updateFromQueue(id: number, requestBody?: Media): Promise<Media>;
    /**
     * @param requestBody
     * @returns any Successful
     * @throws ApiError
     */
    static classify(requestBody: {
        file?: string;
        url?: string;
        force?: boolean;
        objects?: boolean;
    }): Promise<{
        classifications?: Array<{
            name?: string;
            confidence?: number;
            rank?: number;
        }>;
        detections?: Array<{
            mask?: string;
            area?: number;
            boundingBox?: BoundingBox;
            confidence?: number;
            attributes?: {
                'is-stainless'?: boolean;
            };
        }>;
    }>;
    /**
     * @returns any Successful
     * @throws ApiError
     */
    static classifyReso(): Promise<{
        value?: Array<{
            Media?: Array<{
                MediaCategory?: string;
                MediaURL?: string;
                MediaKey?: string;
                MediaModificationTimestamp?: string;
                MediaOf?: string;
            }>;
        }>;
    }>;
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns void
     * @throws ApiError
     */
    static reportClassificationError(requestBody?: {
        labels?: Array<string>;
        hash?: string;
    }): Promise<void>;
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns number number of affected rows
     * @throws ApiError
     */
    static resetByHash(requestBody?: MediaPipelinePayload): Promise<number>;
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns number number of affected rows
     * @throws ApiError
     */
    static updateByHash(requestBody?: MediaPipelinePayload): Promise<number>;
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns number number of affected rows
     * @throws ApiError
     */
    static uploadBatch(requestBody?: {
        propertyId?: string;
        medias?: Array<Media>;
    }): Promise<number>;
    /**
     * For Foyer internal use.
     * @param requestBody
     * @returns any Successful
     * @throws ApiError
     */
    static validateReso(requestBody?: string): Promise<{
        metadata?: {
            run?: number;
            passed?: number;
            failed?: number;
        };
        tests?: {
            passed?: Array<{
                media?: Array<string>;
                tag?: string;
                human_problem_text?: string;
                recommendation?: string;
            }>;
            failed?: Array<{
                media?: Array<string>;
                tag?: string;
                human_problem_text?: string;
                recommendation?: string;
            }>;
        };
        media?: Array<MediaPipelinePayload>;
    }>;
}

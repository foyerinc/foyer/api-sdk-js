import type { PatchBody } from '../models/PatchBody';
import type { Share } from '../models/Share';
export declare class ShareService {
    /**
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static getShares(filter?: string): Promise<Array<Share>>;
    /**
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static postShares(requestBody: Share): Promise<Share>;
    /**
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static makeShare(requestBody: {
        propertyId?: number;
        phoneNumber?: string;
        firstName?: string;
        lastName?: string;
    }): Promise<Share>;
    /**
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static findOne(filter: string): Promise<Share>;
    /**
     * @param id
     * @param filter
     * @returns Share Successful
     * @throws ApiError
     */
    static getShareById(id: number, filter?: string): Promise<Share>;
    /**
     * @param id
     * @param requestBody
     * @returns Share Successful
     * @throws ApiError
     */
    static putShareById(id: number, requestBody: Share): Promise<Share>;
    /**
     * @param id
     * @param requestBody
     * @returns Share patched Share
     * @throws ApiError
     */
    static patchShareById(id: number, requestBody?: PatchBody): Promise<Share>;
    /**
     * @param id
     * @returns void
     * @throws ApiError
     */
    static deleteShareById(id: number): Promise<void>;
}

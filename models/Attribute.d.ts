export declare type Attribute = {
    name?: string;
    confidence?: number;
    value?: Array<number>;
};

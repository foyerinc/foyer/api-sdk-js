/**
 * A JSONPatch document as defined by RFC 6902
 */
export declare type PatchDocument = {
    /**
     * The operation to be performed
     */
    op: PatchDocument.op;
    /**
     * A JSON-Pointer
     */
    path: string;
    /**
     * The value to be used within the operations.
     */
    value?: string;
    /**
     * A string containing a JSON Pointer value.
     */
    from?: string;
};
export declare namespace PatchDocument {
    /**
     * The operation to be performed
     */
    enum op {
        ADD = "add",
        REMOVE = "remove",
        REPLACE = "replace",
        MOVE = "move",
        COPY = "copy",
        TEST = "test"
    }
}

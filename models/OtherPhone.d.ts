export declare type OtherPhone = {
    id?: number;
    OtherPhoneKeyNumeric?: number;
    ResourceRecordKeyNumeric?: number;
    OtherPhoneExt?: string;
    OtherPhoneKey?: string;
    OtherPhoneNumber?: string;
    ResourceRecordID?: string;
    ResourceRecordKey?: string;
    ClassName?: string;
    OtherPhoneType?: string;
    ResourceName?: string;
    ModificationTimestamp?: string;
};

export declare type SocialMedia = {
    id?: number;
    ResourceRecordKeyNumeric?: number;
    SocialMediaKeyNumeric?: number;
    ResourceRecordID?: string;
    ResourceRecordKey?: string;
    SocialMediaKey?: string;
    SocialMediaUrlOrId?: string;
    ClassName?: string;
    ResourceName?: string;
    SocialMediaType?: string;
    ModificationTimestamp?: string;
};

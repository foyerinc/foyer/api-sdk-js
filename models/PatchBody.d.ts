import type { PatchDocument } from './PatchDocument';
export declare type PatchBody = Array<PatchDocument>;

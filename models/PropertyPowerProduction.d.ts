import type { Property } from './Property';
export declare type PropertyPowerProduction = {
    id?: number;
    ListingKeyNumeric?: number;
    PowerProductionAnnual?: number;
    PowerProductionKeyNumeric?: number;
    PowerProductionSize?: number;
    PowerProductionYearInstall?: number;
    ListingId?: string;
    ListingKey?: string;
    PowerProductionKey?: string;
    PowerProductionAnnualStatus?: string;
    PowerProductionType?: string;
    ModificationTimestamp?: string;
    Property?: Property;
};

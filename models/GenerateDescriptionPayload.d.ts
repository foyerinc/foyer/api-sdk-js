export declare type GenerateDescriptionPayload = {
    features?: Array<string>;
    bedrooms?: number;
    bathrooms?: number;
};

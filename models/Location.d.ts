import type { Coordinates } from './Coordinates';
export declare type Location = {
    type: Location.type;
    coordinates: Coordinates;
};
export declare namespace Location {
    enum type {
        POINT = "Point"
    }
}

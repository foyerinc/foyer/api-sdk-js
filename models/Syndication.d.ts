import type { Member } from './Member';
import type { Office } from './Office';
import type { Publisher } from './Publisher';
export declare type Syndication = {
    id?: number;
    memberId?: number;
    officeId?: number;
    publisherId?: number;
    permission?: string;
    createdAt?: string;
    updatedAt?: string;
    Member?: Member;
    Office?: Office;
    Publisher?: Publisher;
};

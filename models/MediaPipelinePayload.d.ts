export declare type MediaPipelinePayload = {
    hash?: string;
    width?: number;
    height?: number;
    quality?: number;
    classifier?: Array<{
        name?: string;
        confidence?: number;
        classId?: number;
        rank?: number;
        source?: string;
    }>;
    segmaps?: Array<{
        class?: number;
        score?: number;
        bbox?: Array<number>;
        mask?: Array<Array<number>>;
        className?: string;
        source?: string;
    }>;
};
